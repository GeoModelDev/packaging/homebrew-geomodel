# set input data
TAG="4.3.1"

#MACOS="big_sur"
MACOS="arm64_monterey"

GMUSER="geomodel"
BOTTLESPATH="/eos/project-g/geomodel/www/sources/bottles/"

export HOMEBREW_NO_INSTALLED_DEPENDENTS_CHECK=1 # to disable the build of the dependents packages, since we still have to update and build them

#HOMEBREWPATH=/usr/local/Homebrew
HOMEBREWPATH=/opt/homebrew

#sh ${HOMEBREWPATH}/Library/Taps/atlas/homebrew-geomodel/scripts/step1_preparation_update_all_formulae_to_new_tag.sh $TAG
#sh ${HOMEBREWPATH}/Library/Taps/atlas/homebrew-geomodel/scripts/step2_build_all_bottles.sh
#sh ${HOMEBREWPATH}/Library/Taps/atlas/homebrew-geomodel/scripts/step3_rename_all_bottles.sh
sh ${HOMEBREWPATH}/Library/Taps/atlas/homebrew-geomodel/scripts/step4_replace_sha_in_formula.sh $TAG $MACOS
#sh ${HOMEBREWPATH}/Library/Taps/atlas/homebrew-geomodel/scripts/step5_upload_all_bottles_to_remote_server.sh $TAG $GMUSER $BOTTLESPATH

# Now we commit and push the changes we made to our brew Git repository
#cd ${HOMEBREWPATH}/Library/Taps/atlas/homebrew-geomodel
#git add .
#git commit -m "Update formula to tag ${TAG}"
#git push

