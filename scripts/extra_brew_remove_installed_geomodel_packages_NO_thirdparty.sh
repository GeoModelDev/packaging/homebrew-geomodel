# it picks all installed GeoModel packages, except for the "thirdparty", and removes them
brew list | grep geomodel | grep -v thirdparty | xargs brew remove
