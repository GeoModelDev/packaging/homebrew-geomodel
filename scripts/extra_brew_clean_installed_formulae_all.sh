
# soft cleaning
#brew remove geomodel geomodel-tools geomodel-visualization geomodel-geomodelg4 geomodel-fullsimlight

# hard cleaning
brew uninstall --force --ignore-dependencies geomodel geomodel-tools geomodel-visualization geomodel-geomodelg4 geomodel-fullsimlight geomodel-fsl geomodel-extensions-atlaslar geomodel-extensions-atlas \
geomodel-thirdparty-coin \
geomodel-thirdparty-soqt \
geomodel-thirdparty-simage \
geomodel-thirdparty-geant4 \
geomodel-thirdparty-hepmc3 \
geomodel-thirdparty-lhapdf \
geomodel-thirdparty-pythia \


