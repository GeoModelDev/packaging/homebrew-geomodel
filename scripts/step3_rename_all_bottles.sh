#!/bin/bash
#
# ---
# Rename bottle filenames:
# ---
# This replaces the double hiphen in the bottle filename 
# from the Homebrew `brew install --build-bottle` command output 
# to a single hiphen, as expected by `brew install`
#
# E.g., from: 
#   geomodel--4.2.0.big_sur.bottle.2.tar.gz 
# to:
#   geomodel-4.2.0.big_sur.bottle.tar.gz 
#
#
# Written by: Riccardo Maria BIANCHI - riccardo.maria.bianchi@cern.ch 
# Created: April 2021
# Last updated on: May 2021
# -------------------------------------------------------
#



# see: https://stackoverflow.com/a/24103055
#for f in *--*.bottle.tar.gz; do echo mv "$f" "${f/--/-}"; done # dry run, for tests: it echoes the command
for f in *--*.bottle.tar.gz; do mv "$f" "${f/--/-}"; done # actual command

