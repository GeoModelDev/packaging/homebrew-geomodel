#!/bin/bash
#
# A shell script to upload Homebrew bottles to a remote server.
#
# Input arguments:
# - The revision tag bottles have been built for: e.g., '4.2.0'. 
#   The tag is used to pick the right bottle files.
# - The name of the user with write access to the remote server. 
#   This is used to access the remote server through SCP
# - The remote path. 
#   This is the path passed to SCP, where bottles will stored.
#
# Written by: Riccardo Maria BIANCHI - riccardo.maria.bianchi@cern.ch 
# Created: April 2021 
# Last updated on: Dec 2022
# -------------------------------------------------------
#

# === INPUT DATA ===
GM_TAG=$1
GM_USER=$2
GM_PATH=$3

if [[ -z "${GM_TAG}" ]]; then echo  "ERROR - tag is missing"; exit; fi
if [[ -z "${GM_USER}" ]]; then echo "ERROR - user is missing"; exit; fi
if [[ -z "${GM_PATH}" ]]; then echo "ERROR - path is missing"; exit; fi


# get the actual folder of the script, not the one from which it is run
__dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

# === FILES LIST ===
GM_FORMULA_LIST=$(cat ${__dir}/filelist.txt);

for gm_formula in $GM_FORMULA_LIST; do
  SUB='thirdparty'
  if [[ "$gm_formula" == "#"* ]]; then
    echo "skipping commented formula: '$gm_formula'..."
  elif [[ "$gm_formula" == *"$SUB"* ]]; then
    echo "Handling a 'thirdparty' formula..."
    for gm_file in ${gm_formula}-*.bottle.tar.gz; do
        if [ -f "$gm_file" ]; then
            scp ${gm_file} ${GM_USER}@lxplus.cern.ch:${GM_PATH}
        else
            echo "   >> skipping $gm_file, it does not exist in the current folder..."
        fi
    done
  else
    echo "Handling a GeoModel formula..."
    for gm_file in ${gm_formula}-${GM_TAG}*.bottle.tar.gz; do
        if [ -f "$gm_file" ]; then
            scp ${gm_file} ${GM_USER}@lxplus.cern.ch:${GM_PATH}
        else
            echo "   >> skipping $gm_file, it does not exist in the current folder..."
        fi
    done
  fi 
done

