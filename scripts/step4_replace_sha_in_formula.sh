#!/bin/bash
#
# A shell script to replace the SH256 checksum of the bottles in 
# the Homebrew formula 
# 
# Written by: Riccardo Maria BIANCHI - riccardo.maria.bianchi@cern.ch 
# Created: April 2021
# Last updated on:
# - 2022 Feb, R.M.Bianchi <riccardo.maria.bianchi@cern.ch>, Added error handling
# - 2021 May, R.M.Bianchi <riccardo.maria.bianchi@cern.ch>
# -------------------------------------------------------
#
usage() {
        [ $# -eq 0 ] && { echo "\nUsage: $(basename $0) tag macos \n"; exit 1; }
    }

# Enable the err trap, code will get called when an error is detected
set -o errtrace 
trap "echo ERROR: There was an error in ${FUNCNAME-main context}, details to follow" ERR

# === INPUT DATA ===
tag=$1    # e.g., '4.2.6'
macos=$2  # e.g., 'big_sur'

# check the input arguments
if [[ -z "${tag}" ]]; then echo "ERROR - the 'tag' argument is missing"; usage; exit; fi
if [[ -z "${macos}" ]]; then echo "ERROR - the 'macos' argument is missing"; usage; exit; fi


# get the actual folder of the script, not the one from which it is run
__dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

# === FILES LIST ===
GM_FORMULA_LIST=$(cat ${__dir}/filelist.txt);

#### test
###echo brew test-bot --root-url=https://geomodel.web.cern.ch/sources/bottles --tap=atlas/geomodel $GM_FORMULA_LIST

if [ -z "$GM_FORMULA_LIST" ]
then
    echo "ERROR! \$GM_FORMULA_LIST is empty"
else
    for formula in $GM_FORMULA_LIST; do
  if [[ "$formula" == "#"* ]]; then
    echo "skipping commented formula: '$formula'..."
  else  
        echo "\n--'${formula}' - Calling the script that replaces the bottle's SHA256 in the formula..."
        sh ${__dir}/stepSingle_replace_bottle_sha256_in_formula.sh ${formula} ${tag} ${macos}
  fi  
  done
  echo "OK! All SHA256 checksums have been replaced.\n"
fi

