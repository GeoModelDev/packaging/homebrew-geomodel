#!/bin/bash
#
# A shell script to update GeoModel Homebrew formula to a specific revision tag, 
#   and update formulas with the new sha256 of the downloaded sources.
#
# Written by: Riccardo Maria BIANCHI - riccardo.maria.bianchi@cern.ch 
# Created:         April 2021
# Updates:
# - 2022 Feb, riccardo.maria.bianchi@cern.ch - added error handling
# - 2021 May, riccardo.maria.bianchi@cern.ch
# -------------------------------------------------------
#

usage() {
    [ $# -eq 0 ] && { echo "\nUsage: $(basename $0) formula tag macosVersion \n"; exit 1; }
}

# Enable the err trap, code will get called when an error is detected
set -o errtrace 
trap "echo ERROR: There was an error in ${FUNCNAME-main context}, details to follow" ERR


# === INPUT DATA ===
formula=$1
tag=$2
macos=$3

# === UPDATE THE FORMULA ===
#
# for TESTING, just remove the "-i '' " (where "-i" means "in place")!
# NOTE: the empty '' backup string after "-i" is needed on macOS, for the -i option to work
#

# get the actual folder of the script, not the one from which it is run
__dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

# --- Get the new sha256 of the bottle we have just built
echo "getting the new SHA256 of the bottle for the formula '${formula}'..." 
newSha256=$(sh ${__dir}/stepSingle_get_bottle_sha256_from_json.sh $formula $tag $macos)
echo "\tFormula: '${formula}' -- This is the new sha256 '${newSha256}' for the macOS '${macos}'"

# --- Update the sha256 of the sources' download
formula_file="${__dir}/../Formula/${formula}.rb"

echo "checking if a line for ${macos} exists in the formula file..."
TEST_OS=$(awk "/${macos}/" ${formula_file})

if [[ -z $TEST_OS ]]; then
  echo "There is no line for $macos!"
  OS_STR="    sha256 cellar: :any, ${macos}: \"${newSha256}\" "
  echo "adding line for the new OS: ${OS_STR}"
  sed -i '' "/bottle do/,/end/ s#\(^[[:blank:]]*end.*\).*#$OS_STR\n\1#" $formula_file
else
  echo "OK, line found for $macos. Replacing the SHA256 ('${newSha256}')..."
  if [ "$newSha256" = "" ];
  then
    echo "ERROR! The new SHA256 string is empty! I do not replace the old hash now! Returning..."
  else
    echo "String is not empty."
    echo "replacing the '${macos}' bottle's SHA256 in file: '${formula_file}'..." 
    sed -i ''  "/bottle do/,/end/ s#\(^[[:blank:]]*sha256.*$macos.*[\"]\).*\([\"].*\)\$#\1$newSha256\2#" $formula_file
  fi
fi


