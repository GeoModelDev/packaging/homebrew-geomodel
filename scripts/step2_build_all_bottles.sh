#!/bin/bash
#
# A shell script to build Homebrew 'bottles' for all GeoModel formula, 
# and update all formulas with the new bottle sha256 ID.
# 
# Written by: Riccardo Maria BIANCHI - riccardo.maria.bianchi@cern.ch 
# Created: April 2021
# Last updated on:
# - 2022 Feb, R.M.Bianchi <riccardo.maria.bianchi@cern.ch>, Added error handling
# - 2021 May, R.M.Bianchi <riccardo.maria.bianchi@cern.ch>
# -------------------------------------------------------
#

# === SETUP ===
# Enable the err trap, code will get called when an error is detected
set -o errtrace 
trap "echo ERROR: There was an error in ${FUNCNAME-main context}, details to follow..." ERR

# get the actual folder of the script, not the one from which it is run
__dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"



# === FILES LIST ===
GM_FORMULA_LIST=$(cat ${__dir}/filelist.txt);

#### test
###echo brew test-bot --root-url=https://geomodel.web.cern.ch/sources/bottles --tap=atlas/geomodel $GM_FORMULA_LIST

for formula in $GM_FORMULA_LIST; do
  if [[ "$formula" == "#"* ]]; then
    echo "skipping commented formula: '$formula'..."
  else  
    echo "\n--'${formula}' - Calling the script that builds the bottle..."
    sh ${__dir}/stepSingle_build_bottle_from_formula.sh ${formula}
  fi
done 
echo "Bottles have been built.\n"

