#!bin/bash
##
#  Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
##
#
# A shell script to update GeoModel Homebrew formula to a specific revision tag, 
#   and update formulas with the new sha256 of the downloaded sources.
#
# Written by: Riccardo Maria BIANCHI - riccardo.maria.bianchi@cern.ch 
# Created:         April 2021
# Updates:
# - 2022 Feb, riccardo.maria.bianchi@cern.ch - Added error handling
# - 2021 May, riccardo.maria.bianchi@cern.ch
# - 2022 Oct, riccardo.maria.bianchi@cern.ch - Added support to update extension formula 
# -------------------------------------------------------
#

usage() {
    [ $# -eq 0 ] && { echo "\nUsage: sh $(basename $0) tag \n"; exit 1; }
}

# Enable the err trap, code will get called when an error is detected
set -o errtrace 
trap "echo ERROR: There was an error in ${FUNCNAME-main context}, details to follow" ERR


# === INPUT DATA ===
GM_TAG=$1 # e.g.: "4.2.0"

if [[ -z "${GM_TAG}" ]]; then echo "ERROR - tag is missing"; usage; exit; fi

# get the actual folder of the script, not the one from which it is run
__dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"


# === FILES LIST ===
GM_FILELIST=$(cat ${__dir}/filelist.txt);

# === COMPUTE THE URL ===
GM_TAG_URL="https://gitlab.cern.ch/GeoModelDev/GeoModel/-/archive/${GM_TAG}/GeoModel-${GM_TAG}.zip"

# === COMPUTE THE SHA256 ===
echo "downloading the sources for tag ${GM_TAG} and computing the SHA256 checksum..."
curl -L $GM_TAG_URL > output.zip
GM_TAG_SHA=$(shasum -a 256 output.zip | awk '{print $1}') # compute the sha256 with 'shasum' and catch it with 'awk' [I could also use: `cut -d' ' -f1`]
rm -f output.zip

# === GET THE COMMIT HASH OF THE TAG ===
GM_TAG_HASH=`curl -Ss --request GET "https://gitlab.cern.ch/api/v4/projects/95156/repository/tags" | jq -r '.[0] | .commit.id'`
echo "The commit hash of the '${GM_TAG}' tag: '${GM_TAG_HASH}' (taken through the GitLab API)"



for file in ${GM_FILELIST}
do
  SUB='thirdparty'
  if [[ "$file" == "#"* ]]; then
    echo "skipping commented formula: '$file'..."
  elif [[ "$file" == *"$SUB"* ]]; then
    echo "It's a 'thirdparty' formula. Calling 'brew install --build-bottle' on it directly..."
    brew install --build-bottle $file
  elif [[ "$file" != "geomodel-extensions-"* ]]; then
    echo "updating BASE formula: ${file}.rb ..."
    sh ${__dir}/stepSingle_replace_tag_in_formula.sh $GM_TAG_URL $GM_TAG_SHA "${__dir}/../Formula/${file}.rb"
  else
    echo "updating EXTENSION formula: ${file}.rb ..."
    sh ${__dir}/stepSingle_replace_tag_in_formula_extension.sh $GM_TAG $GM_TAG_HASH "${__dir}/../Formula/${file}.rb"
  fi
done


