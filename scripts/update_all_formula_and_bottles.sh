#!/bin/bash
##
#  Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
##

## 
# This scripts automates the update to a given tag of all the GeoModel Homebrew formula and bottles
#
# Author: Riccardo Maria BIANCHI <riccardo.maria.bianchi@cern.ch>
#
# Update: 2022, Apr
##

usage() {
    [ $# -eq 0 ] && { echo -e "\nUsage: sh $(basename $0) tag [username] \n"; exit 1; }
}

# input data
GM_TAG=$1 # e.g., "4.4.1"
if [[ -z "${GM_TAG}" ]]; then echo -e "\nERROR! - tag is missing"; usage; exit; fi

GM_USER=$2
if [[ -z "${GM_USER}" ]]; then echo -e "\nINFO - the optional [username] is missing, so we will use 'geomodel' as the default username for uploading the bottles to the CERN servers."; GM_USER="geomodel"; fi

MACOS="arm64_ventura" # "arm64_monterey" # "big_sur"
#MACOS="big_sur"
BOTTLESPATH="/eos/project-g/geomodel/www/sources/bottles/"



### DEPS
if ! command -v jq &> /dev/null
then
    echo "ERROR! ==> 'jq' could not be found. Please install it first (e.g., with: 'brew install jq'). Exiting."
    exit
fi



BREWPATH="/usr/local/Homebrew"
if [[ "$MACOS" =~ .*arm64.* ]]; then
    echo "We build on Apple M1 chip (ARM64); 'brew' on that platform is installed on '/opt/homebrew'"
    BREWPATH="/opt/homebrew"
fi

export HOMEBREW_NO_INSTALLED_DEPENDENTS_CHECK=1 # to disable the build of the dependents packages, since we still have to update and build them


sh ${BREWPATH}/Library/Taps/atlas/homebrew-geomodel/scripts/step1_preparation_update_all_formulae_to_new_tag.sh $GM_TAG
sh ${BREWPATH}/Library/Taps/atlas/homebrew-geomodel/scripts/step2_build_all_bottles.sh
sh ${BREWPATH}/Library/Taps/atlas/homebrew-geomodel/scripts/step3_rename_all_bottles.sh
sh ${BREWPATH}/Library/Taps/atlas/homebrew-geomodel/scripts/step4_replace_sha_in_formula.sh $GM_TAG $MACOS
sh ${BREWPATH}/Library/Taps/atlas/homebrew-geomodel/scripts/step5_upload_all_bottles_to_remote_server.sh $GM_TAG $GM_USER $BOTTLESPATH 

# Now we commit and push the changes we made to our brew Git repository
#cd ${BREWPATH}/Library/Taps/atlas/homebrew-geomodel
#git add .
#git commit -m "Update to tag ${GM_TAG} and ${MACOS}"
#git push
