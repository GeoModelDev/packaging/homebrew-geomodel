#!/bin/bash
#
# A shell script to update an Homebrew formula to a specific revision tag, 
#   and update formulas with the new tag URL and the sha256 of the sources.
#
# Written by: Riccardo Maria BIANCHI - riccardo.maria.bianchi@cern.ch 
# Created:         April 2021
# Last updated on: May 2021
# -------------------------------------------------------
#

# === INPUT DATA ===
tag=$1 # e.g., "4.2.0"
tag_hash=$2
formula_file=$3 

# get the actual folder of the script, not the one from which it is run
__dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"


# === UPDATE THE FORMULA ===
#
# for TESTING, just remove the '-i' ("in place)!
#
# --- Update the Tag name/number
#     NOTE: we exclude the region 'head do...end',
#sed -i ''  "/head do/,/end/! s#\(^[[:blank:]]*tag:.*[\"]\).*\([\"].*\)\$#\1$tag\2#" $formula_file
sed -i ''  "/head do/,/end/! s#\(^[[:blank:]]*version:.*[\"]\).*\([\"].*\)\$#\1$tag\2#" $formula_file

# --- Update the Tag revision/hash
#     NOTE: we exclude the region 'head do...end',
sed -i ''  "/head do/,/end/! s#\(^[[:blank:]]*revision:.*[\"]\).*\([\"].*\)\$#\1$tag_hash\2#" $formula_file


#
# --- Update the sha256 of the sources' download
#     NOTE: we exclude the region 'bottle do...end', so we only get the 'sha256' for the sources, 
#           and not those of the bottles
#sed -i ''  "/bottle do/,/end/! s#\(^[[:blank:]]*sha256.*[\"]\).*\([\"].*\)\$#\1$tag_sha256\2#" $formula_file
#
# ------------
# 'sed' notes: 
# - Since I want to use the script variable, 
#   I have to use double quotes here for sed, 
#   otherwise the variable is not expanded
# - Also, since I use double quotes for the sed command, 
#   I have to escape the double quotes inside the regex patterns
# - Apparently, the dollar sign '$' has to be escaped if inside a sed double-quoted command 
#   (Apparently --> I get errors if I do not do that)
# - Since the expanded variable contains slashes (because it's an URL),
#   I have to replace the default sed delimiter '/' with something else,
#   and I chose to use '#'
# - Also, notice that I used two different delimiters in the command; 
#   That is due to sed that requires a compulsory backslash '\' at the beginning of
#   a search pattern when a delimiter other than the default '/' is used. 
#   Therefore, when using two patterns in an address range, I would have to write
#   something like this:  \#patternA#,\#patternB#
#   So, for simplicity and readiness, I used the default '/' for the search patterns used for 
#   the address range (where no slashes are present in the patterns I use), 
#   and the alternative delimiter for the substitute commands.





### 'SED' DETAILS ###

# Step A - Works, but replaces the url in 'head' too:
#sed '/url/s/".*"/"NEW VERSION NAME"/' $1

# Step B - This works fine, but what if more spaces are present between 'url' and the starting double quote??
#sed '/head do/,/end/! s/url ".*"/url "PIPPO"/' $1

# Step C - OK! Only changes the url inside the sources and with any number of spaces
#sed '/head do/,/end/! s/\(^.*url.*"\).*\(".*\)$/\1PIPPO\2/' $1
#
# --- Explanation:
#
# /head do/,/end/!
#    This is an 'address range', it says to sed to only consider lines between 'head do' and 'end'.
#    Notice, I negate that; so we actually select the lines NOT in that address range.
# s/BLAH/PIPPO/
#    This is the 'substitute' command: it tells sed to replace BLAH with PIPPO
# In details:
# BLAH:
#    \(^.*url.*"\).*\(".*\)$
#       .*url.*"  --> this match a string starting with any characters, then 'url', then any char, then a double quote "
#       But notice I have put that between wto parentheses \( ... \) --> That declare a 'matched group'.
#       So:
#    \(^.*url.*"\) --> means, match the substring containing 'url', between the start of the line and a double quote " INCLUDED,
#                      and save it for later use
#    
#       The same for the following matched group on the same regex:
#    \(".*\)$ --> means, match the substring between a double quote " INCLUDED and the end of the line
#                 and save that for later use
#
# So, used together, the whole regex to declare the text to be substituted, means:
#    take everything (cointaining 'url') up to the double quote and save it, 
#    then take also the text between the double quotes but do not save it,
#    then take the text from the ending double quote to the end of the line and save it.
# Notice that the only part outside a matched group is the substring between the double quotes.
# 
# Then, in the replace part, I use:
#
#    \1PIPPO\2 --> the \1 and \2 are placeholders for the matched groups caught in the first part
#
# So, since the only text not caught in matched groups is the text between the double quotes, 
# only that will be replaced with the text 'PIPPO'
#
# Last modification: I used '^\s*url'  to match 'url' but not 'root_url'. 
# The pattern I used before, '^.*url' matched 'root_url' as well, which is inside the 'bottle do' block.
# NOTE: '\s' is a GNU-sed extension, on macOS, and for a portable solution, I use: '[[:blank:]]'
#
#
# Refs:
# -- sed:
#    - https://unix.stackexchange.com/a/315082
#    - https://stackoverflow.com/a/20808364/320369
# -- sha256:
#    - https://dev.to/slatekit-org/create-a-homebrew-installer-4p09


