#!/bin/bash
#
# A shell script to build an Homebrew 'bottle' for a specific formula,
# and to create a json file storing the bottle's data.
# At the end, it updates the formula file with the new bottle sha256 ID
#
# Input arguments:
# - The name of the formula.
#   This is used to invoke `brew install --build-bottle` upon the right formula.
#
# Written by: Riccardo Maria BIANCHI - riccardo.maria.bianchi@cern.ch 
# Created: April 2021
# Last updated on: May 2021
# -------------------------------------------------------
#

# Verify the type of input and number of values
# Display an error message if the username (input) is not correct
# Exit the shell script with a status of 1 using exit 1 command.
#[ $# -eq 0 ] && { echo "\nUsage: $0 tagNumber [formulaName]"; echo "If 'formulaName' is ommitted, then the bottles for all the GeoModel formulae will be built (default).\n"; exit 1; }
[ $# -eq 0 ] && { echo "\nUsage: $0 formulaName\n"; exit 1; }

# === INPUT DATA ===
GM_FORMULA_NAME=$1

#### test
###echo brew test-bot --root-url=https://geomodel.web.cern.ch/sources/bottles --tap=atlas/geomodel $GM_FORMULA_LIST

# === GET macOS SYSTEM VERSION ===
MACOS_VERSION=$(sw_vers -productVersion | awk '{print $1}')
if [[ $MACOS_VERSION == *"11."* ]]; then
    MACOS_FLAG="big_sur";
elif [[ $MACOS_VERSION == *"10.15"* ]]; then
    MACOS_FLAG="catalina"
elif [[ $MACOS_VERSION == *"10.14"* ]]; then
    MACOS_FLAG="mojave"
elif [[ $MACOS_VERSION == *"12"* ]]; then
    MACOS_FLAG="monterey"
elif [[ $MACOS_VERSION == *"13"* ]]; then
    MACOS_FLAG="ventura"
else
    echo "\n\n*****"
    echo "\n\n***** ERROR!!! macOS version not recognized! --> '${MACOS_VERSION}' "
    echo "*****\n\n"
    exit 1;
fi

# === GET macOS SYSTEM VERSION ===
MAC_CHIP_VERSION=$(sysctl -a | grep machdep.cpu.brand_string)
MAC_CHIP="" 
if [[ $MACOS_VERSION == *"Apple M1"* ]]; then
    MAC_CHIP="arm64_"
fi
#echo "Building bottle of for macOS '$MACOS_FLAG' on '$MAC_CHIP_VERSION'"


echo "\nBuilding a bottle for:  formula '${GM_FORMULA_NAME}' / platform '${MAC_CHIP}${MACOS_FLAG}' ($MAC_CHIP_VERSION)..."
brew install --build-bottle ${GM_FORMULA_NAME}
brew bottle --root-url=https://geomodel.web.cern.ch/sources/bottles --keep-old --json ${GM_FORMULA_NAME}

#brew bottle --root-url=https://geomodel.web.cern.ch/sources/bottles --keep-old --merge --write --no-commit ${formula}--${GM_TAG}.${MACOS_FLAG}.bottle.json 
# TODO: replace this with a custom script to update the sha256 of newly-created bottles in the formulas; this does not work as I would expect and most of all, I don't want to rely on brew tools to update the formula because this can change under the hood as homebrew is updated automatically. Use 'sed' instead to get the sha256 from the json file and update the fields in the formula

