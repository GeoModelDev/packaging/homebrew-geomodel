[![Monterey](https://img.shields.io/badge/macOS-12%20Monterey-blueviolet)](#)
[![Monterey](https://img.shields.io/badge/Xcode-13-blue)](#)
[![Ventura](https://img.shields.io/badge/macOS-13%20Ventura-blueviolet)](#)
[![Ventura](https://img.shields.io/badge/Xcode-13-blue)](#)


# homebrew-geomodel

Homebrew _tap_ for GeoModel packages/libraries. Here you find _formulas_ to install GeoModel packages with the `brew` command.

_Note:_ If you do not have `brew` installed on your machine, you can find documentation and installation instuctions on the Homebrew website: <https://brew.sh/>

## Installation of the whole GeoModel stack

Follow these instructions to install all packages and run GeoModelExplorer (`gmex`).

When you install everything for the first time, please do:

```
brew tap atlas/geomodel https://gitlab.cern.ch/GeoModelDev/packaging/homebrew-geomodel.git
brew install geomodel-visualization
echo ‘export PATH=“/usr/local/opt/qt/bin:$PATH”’ >> ~/.bash_profile    # this, only the first time you install
gmex
```

Then, if at a later time you want to update your installation to fetch a new version of `gmex`, you can just type:

```
brew update
gmex
```

## Troubleshooting

### Links

If you get warnings like those below while installing a package, that means that relics from a previous installation (or from a previous tentative of installation) are laying around and conflict with the installation.

```
Error: The `brew link` step did not complete successfully
The formula built, but is not symlinked into /usr/local
Could not symlink lib/libGeoModelDBManager.3.1.1.dylib
Target /usr/local/lib/libGeoModelDBManager.3.1.1.dylib
already exists. You may want to remove it:
  rm '/usr/local/lib/libGeoModelDBManager.3.1.1.dylib'

To force the link and overwrite all conflicting files:
  brew link --overwrite geomodelio

To list all files that would be deleted:
  brew link --overwrite --dry-run geomodelio

Possible conflicting files are:
/usr/local/lib/libGeoModelDBManager.3.1.1.dylib
/usr/local/lib/libGeoModelDBManager.3.dylib -> /usr/local/lib/libGeoModelDBManager.3.1.1.dylib
...
...
```

To fix that, just run the command suggested by Homebrew:

```
brew link --overwrite geomodelio
```


## Installation of single packages

If you only want to install single packages, for example for test or development, please follow the installation instructions below.

First, install the ATLAS 'tap':

```
brew tap atlas/geomodel https://gitlab.cern.ch/GeoModelDev/packaging/homebrew-geomodel.git
```

Then, install the desired package as explained in the following.

### GeoModelCore and GeoModelIO

```bash
brew install geomodel
```

### GeoModelTools

```bash
brew install geomodel-tools
```

### GeoModelVisualization (gmex)

```bash
brew install geomodel-visualization
```

### GeoModelG4

```bash
brew install geomodel-geomodelg4
```

### FullSimLight

```bash
brew install geomodel-fullsimlight
```

### GeoModelExamples

_coming soon_


---

## How to update the 'bottles' (pre-compiled binaries)

**Note:** *The instructions below are for the GeoModel maintainers only; end users should not need those.*

### NEW 

```sh
# set input data
TAG="4.2.6"
MACOS="big_sur"
GMUSER="geomodel"
BOTTLESPATH="/eos/project-g/geomodel/www/sources/bottles/"

export HOMEBREW_NO_INSTALLED_DEPENDENTS_CHECK=1 # to disable the build of the dependents packages, since we still have to update and build them 


sh /usr/local/Homebrew/Library/Taps/atlas/homebrew-geomodel/scripts/step1_preparation_update_all_formulae_to_new_tag.sh $TAG
sh /usr/local/Homebrew/Library/Taps/atlas/homebrew-geomodel/scripts/step2_build_all_bottles.sh
sh /usr/local/Homebrew/Library/Taps/atlas/homebrew-geomodel/scripts/step3_rename_all_bottles.sh
sh /usr/local/Homebrew/Library/Taps/atlas/homebrew-geomodel/scripts/step4_replace_sha_in_formula.sh $TAG $MACOS
sh /usr/local/Homebrew/Library/Taps/atlas/homebrew-geomodel/scripts/step5_upload_all_bottles_to_remote_server.sh $TAG $GMUSER $BOTTLESPATH 

# Now we commit and push the changes we made to our brew Git repository 
cd /usr/local/Homebrew/Library/Taps/atlas/homebrew-geomodel
git add .
git commit -m "Update formula to tag ${TAG}"
git push
```

### How to update the Geant4 version for GeoModel 

At first, update the URL of the sources of the desired version of Geant4, in `Formula/geomodel-thirdparty-geant4.rb`.

Then, remove the `geomodel-thirdparty-geant4` package from the build machine:

```sh
brew uninstall --ignore-dependencies geomodel-thirdparty-geant4
```

Then, build the new bottle:

```sh
brew install --build-bottle geomodel-thirdparty-geant4
brew bottle --no-rebuild --json geomodel-thirdparty-geant4
```

The latest command generates two files: the "bottle" and a JSON file containing all the info about the bottle itself and the build.

### How to update the bottles for Simage, Coin, and SoQt

```sh
brew install --build-bottle geomodel-thirdparty-simage
brew bottle --no-rebuild --json geomodel-thirdparty-simage
brew install --build-bottle geomodel-thirdparty-coin
brew bottle --no-rebuild --json geomodel-thirdparty-coin
brew install --build-bottle geomodel-thirdparty-soqt
brew bottle --no-rebuild --json geomodel-thirdparty-soqt
```

### OLD


```sh
# -- set the GeoModel global tag we want to update to 
GM_TAG=4.2.4

# -- create a build directory and access it
mkdir geomodel-brew-"$(date +%F)"-"$(date +%R-%S)" && cd "$_"

# -- Step 1 - update all formula to a new tag
sh /usr/local/Homebrew/Library/Taps/atlas/homebrew-geomodel/scripts/step1_preparation_update_all_formulae_to_new_tag.sh 4.2.4

# -- Step 2 - build all formula 
# a) skip checks
#    Set an env var to prevent Homebrew to check for dependent packages 
#    before the new bottles are built; you get errors otherwise
export HOMEBREW_NO_INSTALLED_DEPENDENTS_CHECK=1
# b) build the bottles 
sh /usr/local/Homebrew/Library/Taps/atlas/homebrew-geomodel/scripts/step2_build_all_bottles.sh

# -- Step 3 - rename all bottles 
sh /usr/local/Homebrew/Library/Taps/atlas/homebrew-geomodel/scripts/step3_rename_all_bottles.sh

# -- Step 4 - replace the bottles' SHA256 checksums within the formula for a single, particular macOS version 
sh /usr/local/Homebrew/Library/Taps/atlas/homebrew-geomodel/scripts/step4_replace_sha_in_formula.sh 4.2.4 big_sur

# -- Step 5 - upload the new bottles to the GeoModel server
# ==> TODO: For this, we should use the 'geomodel' account and a passwordless or non-interactive password method
sh /usr/local/Homebrew/Library/Taps/atlas/homebrew-geomodel/scripts/step5_upload_all_bottles_to_remote_server.sh 4.2.4 geomodel /eos/project-g/geomodel/www/sources/bottles/

# -- Step 6 -- commit and push the updated Formula
# ==> TODO: For this, we should use the 'geomodel' account and a passwordless or non-interactive password method
cd /usr/local/Homebrew/Library/Taps/atlas/homebrew-geomodel/Formula 
git add .
git commit -m "Formula updated to tag ${GM_TAG}"
git push 
```

## Troubleshooting

While in step1, if you get an error like the one below, you should probably update the command-line tools:

```
Error: Your Command Line Tools are too outdated.
Update them from Software Update in System Preferences or run:
  softwareupdate --all --install --force

If that doesn't show you any updates, run:
  sudo rm -rf /Library/Developer/CommandLineTools
  sudo xcode-select --install

Alternatively, manually download them from:
  https://developer.apple.com/download/all/.
You should download the Command Line Tools for Xcode 13.1.

Error: Formula not installed or up-to-date: atlas/geomodel/geomodel
```

You can update the command-line tools by following the suggestions given in the error message; for instance:

```
softwareupdate --all --install --force
```



### OLD 

#### Steps

```
GM_VERSION="4.2.0"
GM_ROOT_WEB_URL="https://geomodel.web.cern.ch/sources/bottles"
GM_ROOT_SCP_URL="lxplus.cern.ch:/eos/project-g/geomodel/www/sources/bottles/"

brew install -v --build-bottle atlas/geomodel/geomodel-tools
brew bottle geomodel-tools
sh rename_bottle.sh
scp geomodel-tools-4.2.0.big_sur.bottle.tar.gz geomodel@lxplus.cern.ch:/eos/project-g/geomodel/www/sources/bottles/
```


#### Automating

Refs:
- `bottle`: https://docs.brew.sh/Manpage#bottle-options-installed_formulafile-
- https://jonathanchang.org/blog/updating-homebrew-formulae-when-your-software-gets-a-new-version/
- https://jonathanchang.org/blog/maintain-your-own-homebrew-repository-with-binary-bottles/
- https://dev.to/slatekit-org/create-a-homebrew-installer-4p09 (sha256)


To build the bottles and get a lot info about the bottles, the Homebrew `test-bot` can be used:

```
brew test-bot --root-url=https://geomodel.web.cern.ch/sources/bottles --tap=atlas/geomodel atlas/geomodel/geomodel-visualization
```

Workflow:

```
cd /usr/local/Homebrew/Library/Taps/atlas/homebrew-geomodel/Formula
sh ../scripts/update_all_formulae.sh 4.2.0
sh ../scripts/build_bottles.sh 4.2.0
sh ../scripts/rename_bottle.sh
sh ../scripts/upload_bottles.sh 4.2.0 <user> <lxplus_path>
```


Also this, but still have to understand how to use it properly:

- it builds the bottles for all formulas, but with a lot of checks... 

```
###brew test-bot --root-url=https://geomodel.web.cern.ch/sources/bottles --tap=atlas/geomodel  geomodel geomodel-tools geomodel-visualization geomodel-geomodelg4 geomodel-fullsimlight
```

## TODO 

### automatic 'merge' 

`brew bottle --merge` can merge the details of another bottle int a bottle block of a formula. 

With `jq` (brew install jq) we can access information from the JSON file created by `brew bottle --json`

For example:

```
$ cat geomodel-thirdparty-geant4--11.0.1.arm64_monterey.bottle.json | jq '.[] | .bottle.tags.arm64_monterey.filename'
"geomodel-thirdparty-geant4-11.0.1.arm64_monterey.bottle.tar.gz"
```
