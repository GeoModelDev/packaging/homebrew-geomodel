class GeomodelThirdpartyCoin < Formula
  desc "The Coin C++ 3D engine"
  homepage "https://github.com/coin3d/coin"

  # SOURCES
  url "https://geomodel.web.cern.ch/sources/coin-4.0.2-src.zip"
  version "4.0.2"
  sha256 "b764a88674f96fa540df3a9520d80586346843779858dcb6cd8657725fcb16f0"
  license "BSD-3-Clause"

  # DEPENDENCIES
  depends_on "cmake" => :build
  depends_on "boost"
  depends_on "atlas/geomodel/geomodel-thirdparty-simage"

  # BUILD INSTRUCTIONS
  def install
    mkdir "builddir" do
      system "cmake", "-DCOIN_BUILD_TESTS=0", "-DCMAKE_CXX_STANDARD=17", "..", *std_cmake_args
      system "make"
      system "make", "install"
    end
  end

  # PRE-COMPILED PACKAGES
  bottle do
    root_url "https://geomodel.web.cern.ch/sources/bottles"
    sha256 cellar: :any, arm64_ventura: "7fe8a08e535a42b890d3275e5d6df67cfe62c1e9d537b4672a9391cb27e3c4ac"
  end

end
