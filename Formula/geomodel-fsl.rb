class GeomodelFsl < Formula

  desc "The GeoModel Tools Suite for High-Energy-Physics Detector Description - FSL, a GUI for FullSimLight"
  homepage "https://gitlab.cern.ch/GeoModelDev/GeoModel"

  
  # SOURCES
  url "https://gitlab.cern.ch/GeoModelDev/GeoModel/-/archive/6.0.0/GeoModel-6.0.0.zip"
  sha256 "d50538507945002a2f4f2a874b993b39237775ccaf7149565cd655b829885615"


  head do
    url "https://gitlab.cern.ch/GeoModelDev/GeoModel.git"
  end

  # DEPENDENCIES
  depends_on "cmake" => :build
  depends_on "qt@5" # TODO: move to 'qt' (i.e., Qt6)
  depends_on "atlas/geomodel/geomodel-fullsimlight"
  
  option "without-hepmc3", "Compile without HepMC3 support. FullSimLight will be fully usable, but you will not be able to use HepMC3 input." 
  
  # A NOTE: GeoModel now uses a private bottle for HepMc3 because, at the time of writing,
  # there are no bottles built for the Apple M1 chip from the original repository.
  # By using a private build, we ensure we always have a bottle built 
  # for the needed version of the third-party package.
  #depends_on "davidchall/hep/hepmc3" => :recommended 
  depends_on "atlas/geomodel/geomodel-thirdparty-hepmc3" => :recommended 



  # INSTALLATION INSTRUCTIONS
  def install

    args = ""
    args << "-DGEOMODEL_USE_HEPMC3=0" if build.without? "geomodel-thirdparty-hepmc3"

    mkdir "build" do
      system "cmake", "-G", "Unix Makefiles", "../FSL", *std_cmake_args, args
      system "make"
      system "make", "install"
   end
  end

 
  bottle do
    root_url "https://geomodel.web.cern.ch/sources/bottles"
    sha256 cellar: :any, arm64_ventura: "7f5c0f115c84388fe20b9aecb8b4d5050402cb506f19f75e1378c3ff6e662b39" 
  end


end
