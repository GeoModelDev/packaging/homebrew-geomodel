# Documentation: https://docs.brew.sh/Formula-Cookbook
#                https://www.rubydoc.info/github/Homebrew/brew/master/Formula
class GeomodelTools < Formula
  desc "The GeoModel Tools Suite for High-Energy-Physics Detector Description - Tools"
  homepage "https://gitlab.cern.ch/GeoModelDev/GeoModel"
  license "Apache-2.0"
  
  # SOURCES
  url "https://gitlab.cern.ch/GeoModelDev/GeoModel/-/archive/6.0.0/GeoModel-6.0.0.zip"
  sha256 "d50538507945002a2f4f2a874b993b39237775ccaf7149565cd655b829885615"

  bottle do
    root_url "https://geomodel.web.cern.ch/sources/bottles"
    sha256 cellar: :any, arm64_ventura: "f4c44196fa07481e64235f1f6c13e0e92bf1eac477d7cc8ee213721a476aa123" 
  end

  head do
    url "https://gitlab.cern.ch/GeoModelDev/GeoModel.git"
  end

  # DEPENDENCIES
  depends_on "cmake" => :build
  depends_on "eigen"
  depends_on "xerces-c"
  depends_on "nlohmann-json"
  depends_on "atlas/geomodel/geomodel"

  
  # INSTALLATION INSTRUCTIONS
  def install
    mkdir "build" do
      system "cmake", "-G", "Unix Makefiles", "../GeoModelTools", *std_cmake_args
      system "make"
      system "make", "install"
   end
  end


  # test do
  #   # `test do` will create, run in and delete a temporary directory.
  #   #
  #   # This test will fail and we won't accept that! For Homebrew/homebrew-core
  #   # this will need to be a test that verifies the functionality of the
  #   # software. Run the test with `brew test geomodelkernel`. Options passed
  #   # to `brew install` such as `--HEAD` also need to be provided to `brew test`.
  #   #
  #   # The installed folder is not in the path, so use the entire path to any
  #   # executables being tested: `system "#{bin}/program", "do", "something"`.
  #   system "false"
  # end
end
