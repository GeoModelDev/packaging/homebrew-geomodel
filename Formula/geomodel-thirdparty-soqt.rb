class GeomodelThirdpartySoqt < Formula
  desc "The SoQt package, allowing Coin to be used with Qt"
  homepage "https://github.com/coin3d/soqt"

  # SOURCES
  url "https://geomodel.web.cern.ch/sources/soqt-1.6.2-src.zip"
  version "1.6.2"
  sha256 "5858acf29007a5518aec6837eee813c9330eab6ec7c13e6a8511b0ee882daaf1"
  license "BSD-3-Clause"

  #head "https://bitbucket.org/rmbianchi/soqt",

  # DEPENDENCIES
  depends_on "cmake" => :build
  depends_on "qt@5" # TODO: move to 'qt' (i.e., Qt6)
  depends_on "atlas/geomodel/geomodel-thirdparty-coin"

  # BUILD INSTRUCTIONS
  def install
    mkdir "builddir" do
      system "cmake", "-DSOQT_BUILD_MAC_X11=0", "-DSOQT_BUILD_TESTS=0",  "..", *std_cmake_args
      system "make"
      system "make", "install"
    end
  end

  # PRE-COMPILED PACKAGES
  bottle do
    root_url "https://geomodel.web.cern.ch/sources/bottles"
    sha256 cellar: :any, arm64_ventura: "4a1d8a27520182bbadbe2cfaea34351cc6b73f18cdfeb3c892011bc21eb62d2f"
  end

end
