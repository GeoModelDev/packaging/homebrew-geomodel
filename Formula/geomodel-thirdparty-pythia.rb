class GeomodelThirdpartyPythia < Formula
  desc "Monte Carlo event generator"
  homepage "https://pythia.org"
  url "https://pythia.org/download/pythia83/pythia8309.tgz"
  version "8.309"
  sha256 "5bdafd9f2c4a1c47fd8a4e82fb9f0d8fcfba4de1003b8e14be4e0347436d6c33"
 
  # Note: 
  # GeoModel uses a customized bottle for Pythia 
  # to always have an up to date version that is well tested 
  # with all the GeoModel tools, and with bottles built for all 
  # the needed OS versions and chips.
  #
  # Note: 
  # this formula has been forked from this original formula by David C Hall:
  # https://github.com/davidchall/homebrew-hep/blob/HEAD/Formula/pythia.rb
  #
  
  license "GPL-2.0-or-later"

  livecheck do
    url "https://pythia.org/releases"
    regex(/href=.*?pythia(\d)(\d{3})\.t/i)
    strategy :page_match do |page, regex|
      page.scan(regex).map { |match| match.join(".") }
    end
  end

  bottle do
    root_url "https://geomodel.web.cern.ch/sources/bottles"
    sha256 arm64_ventura: "e032644e1a482a31ba09f49a26db20280d8008bb59c595defacc7d9af4ec64cc"
  end

  #depends_on "boost"
  depends_on "geomodel-thirdparty-hepmc3"
  depends_on "geomodel-thirdparty-lhapdf"

  def install
    #args = %W[
    #  --prefix=#{prefix}
    #  --with-hepmc3=#{Formula["geomodel-thirdparty-hepmc3"].opt_prefix}
    #  --with-lhapdf6=#{Formula["lhapdf"].opt_prefix}
    #  --with-boost=#{Formula["boost"].opt_prefix}
    #]
    args = %W[
      --prefix=#{prefix}
      --with-hepmc3=#{Formula["geomodel-thirdparty-hepmc3"].opt_prefix}
      --with-lhapdf6=#{Formula["geomodel-thirdparty-lhapdf"].opt_prefix}
    ]

    system "./configure", *args
    system "make"
    system "make", "install"
  end

  test do
    cp_r share/"Pythia8/examples/.", testpath
    system "make", "main01"
    system "./main01"
  end
end


