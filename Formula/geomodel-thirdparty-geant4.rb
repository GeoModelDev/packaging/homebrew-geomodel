class GeomodelThirdpartyGeant4 < Formula
  desc "GeoModel Detector Description Tools Suite -- A tested/validated version of the Geant4 simulation development toolkit"
  homepage "https://geant4.web.cern.ch"
  url "http://cern.ch/geant4-data/releases/geant4-v11.0.1.tar.gz"
  sha256 "3e9b0e68b006c1ddd8c5f6ded084fcd8029a568ecd0e45026d7ef818df46a02b"

  depends_on "cmake" => :build
  depends_on "doxygen" => :build
  depends_on "xerces-c"
   def install
     mkdir "builddir" do
     system "cmake", "..", "-DGEANT4_USE_GDML=ON", "-DGEANT4_USE_SYSTEM_EXPAT=OFF", "-DGEANT4_BUILD_MULTITHREADED=ON", *std_cmake_args
     system "make", "install"

    end
   end

   bottle do
    root_url "https://geomodel.web.cern.ch/sources/bottles"
    sha256 cellar: :any, big_sur: "5aaffa95a331eec3a868d616e1d7e170f59f06fff4d6d31511cde133841d3cd3"
    sha256 cellar: :any, arm64_monterey: "2de3c8f73d1c7776f5ba3b7a7057f7fb2f423ee9402a1b905f6f260520012924"
  end

  def post_install
     system "geant4-config --install-datasets"
  end
end
