class GeomodelThirdpartyGeant4Wed < Formula
  desc "GeoModel Detector Description Tools Suite -- A tested/validated version of the Geant4 simulation development toolkit"
  homepage "https://geant4.web.cern.ch"
  url "https://gitlab.cern.ch/atlas-simulation-team/geant4/-/archive/v10.6.3.5/geant4-v10.6.3.5.zip"
  sha256 "fee5af6892314ab39a50508cef77b4d85e5a9c93c8cfadb6decb0fdd184c41ae"

  depends_on "cmake" => :build
  depends_on "doxygen" => :build
  depends_on "xerces-c"
   def install
     mkdir "builddir" do
     #system "cmake", "..", "-DGEANT4_USE_GDML=ON", "-DGEANT4_USE_SYSTEM_EXPAT=OFF", *std_cmake_args
     system "cmake", "..", "-DGEANT4_USE_GDML=ON", *std_cmake_args
     system "make", "install"

    end
   end

   bottle do
    root_url "https://geomodel.web.cern.ch/sources/bottles"
    sha256 cellar: :any, arm64_monterey: "75e4f4ad6db63383687547c71bf70676a6d626c85b29fc62987c519ee1d7ea61"
  end

  def post_install
     system "geant4-config --install-datasets"
  end
end
