class GeomodelExtensionsAtlaslar < Formula
  desc "The GeoModel Tools Suite for High-Energy-Physics Detector Description - Extensions: ATLAS LAr custom shape"
  homepage "https://gitlab.cern.ch/GeoModelDev/GeoModel"
  license "Apache-2.0"
  # SOURCES 
  # GitHub's archives lack submodules, must pull:
  url "https://gitlab.cern.ch/GeoModelDev/GeoModel.git",
      tag:      "4.4.3",
      revision: "ab8d698a8ed5de46cb72871cdbd679840e6bf792"
  
  #head "https://gitlab.cern.ch/GeoModelDev/GeoModel.git", branch: "master"

  # DEPENDENCIES
  depends_on "cmake" => :build
  depends_on "geomodel"

  # INSTALLATION INSTRUCTIONS
  def install
    mkdir "build" do
      system "cmake", "-G", "Unix Makefiles", "../ATLASExtensions/LArCustomSolidExtension", *std_cmake_args
      system "make"
      system "make", "install"
   end
  end

  # PRE-COMPILED PACKAGES (Bottles)
  bottle do
    root_url "https://geomodel.web.cern.ch/sources/bottles"
    sha256 cellar: :any, arm64_monterey: "1b4a6f907198856efe48adf918c9040b8e77b309fcc8e9b4a62b9e246341be30" 
  end

  # test do
  #   # `test do` will create, run in and delete a temporary directory.
  #   #
  #   # This test will fail and we won't accept that! For Homebrew/homebrew-core
  #   # this will need to be a test that verifies the functionality of the
  #   # software. Run the test with `brew test geomodelkernel`. Options passed
  #   # to `brew install` such as `--HEAD` also need to be provided to `brew test`.
  #   #
  #   # The installed folder is not in the path, so use the entire path to any
  #   # executables being tested: `system "#{bin}/program", "do", "something"`.
  #   system "false"
  # end
end
