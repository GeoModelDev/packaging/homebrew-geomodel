class GeomodelVisualization < Formula
  desc "The GeoModel Tools Suite for High-Energy-Physics Detector Description - FullSimLight"
  homepage "https://gitlab.cern.ch/GeoModelDev/GeoModel"

  # SOURCES
  url "https://gitlab.cern.ch/GeoModelDev/GeoModel/-/archive/6.0.0/GeoModel-6.0.0.zip"
  sha256 "d50538507945002a2f4f2a874b993b39237775ccaf7149565cd655b829885615"

  head do
    url "https://gitlab.cern.ch/GeoModelDev/GeoModel.git"
  end

  # DEPENDENCIES
  depends_on "cmake" => :build
  depends_on "qt@5" # TODO: move to 'qt' (i.e., Qt6)
  depends_on "eigen"
  depends_on "nlohmann-json"
  depends_on "hdf5"
  depends_on "atlas/geomodel/geomodel-thirdparty-simage"
  depends_on "atlas/geomodel/geomodel-thirdparty-coin"
  depends_on "atlas/geomodel/geomodel-thirdparty-soqt"
  depends_on "atlas/geomodel/geomodel"

  
  # INSTALLATION INSTRUCTIONS
  def install
    mkdir "build" do
      system "cmake", "-G", "Unix Makefiles", "../GeoModelVisualization", *std_cmake_args
      system "make"
      system "make", "install"
   end
  end

  # PRE-COMPILED PACKAGES (Bottles)
  bottle do
    root_url "https://geomodel.web.cern.ch/sources/bottles"
    sha256 cellar: :any, arm64_ventura: "7e8ac9ce3fbd280b543a308a92a043c6a46e2d6f8d3df2770ae0a1e34a79d3c4" 
  end

  # test do
  #   # `test do` will create, run in and delete a temporary directory.
  #   #
  #   # This test will fail and we won't accept that! For Homebrew/homebrew-core
  #   # this will need to be a test that verifies the functionality of the
  #   # software. Run the test with `brew test geomodelkernel`. Options passed
  #   # to `brew install` such as `--HEAD` also need to be provided to `brew test`.
  #   #
  #   # The installed folder is not in the path, so use the entire path to any
  #   # executables being tested: `system "#{bin}/program", "do", "something"`.
  #   system "false"
  # end
end
