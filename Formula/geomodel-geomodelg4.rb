class GeomodelGeomodelg4 < Formula

  desc "The GeoModel Tools Suite for High-Energy-Physics Detector Description - GeoModelG4"
  homepage "https://gitlab.cern.ch/GeoModelDev/GeoModel"

  # SOURCES
  url "https://gitlab.cern.ch/GeoModelDev/GeoModel/-/archive/6.0.0/GeoModel-6.0.0.zip"
  sha256 "d50538507945002a2f4f2a874b993b39237775ccaf7149565cd655b829885615"

  head do
    url "https://gitlab.cern.ch/GeoModelDev/GeoModel.git"
  end


  # DEPENDENCIES
  depends_on "cmake" => :build
  depends_on "eigen"
  depends_on "atlas/geomodel/geomodel"
  depends_on "atlas/geomodel/geomodel-thirdparty-geant4" 

  # INSTALLATION INSTRUCTIONS
  def install
    mkdir "build" do
      system "cmake", "-G", "Unix Makefiles", "../GeoModelG4", *std_cmake_args
      system "make"
      system "make", "install"
   end
  end


 bottle do
    root_url "https://geomodel.web.cern.ch/sources/bottles"
    sha256 cellar: :any, arm64_ventura: "e7089c3a78de3253cbf1bfd1c640100af812ad00a2f842d9a4a39dcf932d26c8" 
  end


end
