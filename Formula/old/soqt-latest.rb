class SoqtLatest < Formula
  desc "The SoQt package, allowing Coin to be used with Qt"
  homepage "https://bitbucket.org/Coin3D/SoQt"

  # SOURCES
  url "http://atlas-vp1.web.cern.ch/atlas-vp1/sources/soqt-1.6.0-src_75d4766_2020Apr12.zip"
  sha256 "d69db098ddf5e6151c9b525a0366e6e5c023ff26e7f9c73f31a1e6efd440a1d9"

  #head "https://bitbucket.org/rmbianchi/soqt",

  # DEPENDENCIES
  depends_on "atlas/geomodel/cmake-geomodel" => :build
  depends_on "qt5"
  depends_on "atlas/geomodel/coin-geomodel"

  # BUILD INSTRUCTIONS
  def install
    mkdir "builddir" do
      system "cmake", "..", *std_cmake_args
      system "make"
      system "make", "install"
    end
  end

  # PRE-COMPILED PACKAGES
  bottle do
    root_url "http://atlas-vp1.web.cern.ch/atlas-vp1/sources/bottles"
    cellar :any
    sha256 "779871503cfbf47c86c01ba8dbe7fa97a3064f20aa683b32da370ba3fbf9708f" => :mojave
  end

end
