# Documentation: https://docs.brew.sh/Formula-Cookbook
#                https://www.rubydoc.info/github/Homebrew/brew/master/Formula
class Geomodeldatamanagers < Formula
  desc ""
  homepage "https://gitlab.cern.ch/GeoModelATLAS/GeoModelDataManagers/"

  # SOURCES
  url "https://gitlab.cern.ch/GeoModelATLAS/GeoModelDataManagers/-/archive/1.0.0/GeoModelDataManagers-1.0.0.zip"
  sha256 "8943b6fd355884d73e05abbefed20e8718de0d3ac94da2bd5ee29bbc48c5b304"
  head do
    url "https://gitlab.cern.ch/GeoModelATLAS/GeoModelDataManagers.git"
  end

  # DEPENDENCIES
  depends_on "cmake" => :build
  depends_on "eigen"
  depends_on "xerces-c"
  depends_on "atlas/geomodel/geomodelcore"
  depends_on "atlas/geomodel/geomodeltools"

  # INSTALLATION INSTRUCTIONS
  def install
    mkdir "build" do
      system "cmake", "-G", "Unix Makefiles", "..", *std_cmake_args
      system "make"
      system "make", "install"
   end
  end

  # PRE-COMPILED PACKAGES (Bottles)
  bottle do
    root_url "http://atlas-vp1.web.cern.ch/atlas-vp1/sources/bottles"
    cellar :any
    # rebuild 2
    sha256 "c3fb516786612aa8249a15705f36401b2594fb50e9a7d0960038393ad5b96200" => :mojave
  end

  # test do
  #   # `test do` will create, run in and delete a temporary directory.
  #   #
  #   # This test will fail and we won't accept that! For Homebrew/homebrew-core
  #   # this will need to be a test that verifies the functionality of the
  #   # software. Run the test with `brew test geomodelkernel`. Options passed
  #   # to `brew install` such as `--HEAD` also need to be provided to `brew test`.
  #   #
  #   # The installed folder is not in the path, so use the entire path to any
  #   # executables being tested: `system "#{bin}/program", "do", "something"`.
  #   system "false"
  # end
end
