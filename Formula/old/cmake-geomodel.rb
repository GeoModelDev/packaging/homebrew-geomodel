class CmakeGeomodel < Formula
  desc "Cross-platform make"
  homepage "https://www.cmake.org/"
  
  url "https://github.com/Kitware/CMake/releases/download/v3.18.5/cmake-3.18.5.zip"
  sha256 "6f97ebe5269dd7b6ae983ad6b83d2f22bd9e4e70ec4e57083d004373dfe2a0d7"

  license "BSD-3-Clause"
  revision 1
  head "https://gitlab.kitware.com/cmake/cmake.git"

  livecheck do
    url "https://cmake.org/download/"
    regex(/Latest Release \(v?(\d+(?:\.\d+)+)\)/i)
  end

  bottle do
    root_url "http://atlas-vp1.web.cern.ch/atlas-vp1/sources/bottles"
    cellar :any_skip_relocation
    rebuild 1
    sha256 "4f43766910739c503e7f90bf825ec4513242e8741a4b55dc3a83674cc0d3efcc" => :mojave
  end

  depends_on "sphinx-doc" => :build

  on_linux do
    depends_on "openssl@1.1"
  end

  # Backport patch for 3.19.0: https://gitlab.kitware.com/cmake/cmake/-/issues/21469
  #patch do
  #  url "https://gitlab.kitware.com/cmake/cmake/-/commit/30aa715fac06deba7eaa3e6167cf34eb4d2521d0.patch"
  #  sha256 "471843b53ea5749eda8b32ef69f9ab20c17e0087992ce3bf8cba93e6e87c54b5"
  #end

  # The completions were removed because of problems with system bash

  # The `with-qt` GUI option was removed due to circular dependencies if
  # CMake is built with Qt support and Qt is built with MySQL support as MySQL uses CMake.
  # For the GUI application please instead use `brew cask install cmake`.

  def install
    args = %W[
      --prefix=#{prefix}
      --no-system-libs
      --parallel=#{ENV.make_jobs}
      --datadir=/share/cmake
      --docdir=/share/doc/cmake
      --mandir=/share/man
      --sphinx-build=#{Formula["sphinx-doc"].opt_bin}/sphinx-build
      --sphinx-html
      --sphinx-man
      --system-zlib
      --system-bzip2
      --system-curl
    ]

    system "./bootstrap", *args, "--", *std_cmake_args,
                                       "-DCMake_INSTALL_EMACS_DIR=#{elisp}"
    system "make"
    system "make", "install"
  end

  test do
    (testpath/"CMakeLists.txt").write("find_package(Ruby)")
    system bin/"cmake", "."
  end
end
