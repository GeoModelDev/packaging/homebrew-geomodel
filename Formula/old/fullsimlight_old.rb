class Fullsimlight < Formula

  desc "FullSimLight"
  homepage "https://gitlab.cern.ch/GeoModelDev/FullSimLight"

  url "https://gitlab.cern.ch/GeoModelDev/FullSimLight/-/archive/1.0.1/FullSimLight-1.0.1.tar.gz"
  sha256 "e18eb94447f3726c1b467834bc62714bb882b638d511c4c571253de4872c064b"

  head do
    url "https://gitlab.cern.ch/GeoModelDev/FullSimLight.git"
  end

  # DEPENDENCIES
  depends_on "cmake" => :build
  depends_on "eigen"
  depends_on "nlohmann-json"
  depends_on "atlas/geomodel/geomodel"
  depends_on "atlas/geomodel/geomodelg4"
  depends_on "atlas/geomodel/geant4"

  # INSTALLATION INSTRUCTIONS
  def install
    mkdir "build" do
      system "cmake", "-G", "Unix Makefiles", "..", *std_cmake_args
      system "make"
      system "make", "install"
   end
  end

  bottle do
    root_url "http://atlas-vp1.web.cern.ch/atlas-vp1/sources/bottles"
    cellar :any
    # sha256 "589fec0d165c38e220aea50ed8a0a81d0649b502c73547cae90586b1dc6a2cb8" => :catalina
    sha256 "889533b6771a7bd7106abede59391e5d19f7e2556fa46e25222f7d28d61411c1" => :mojave
  end


end
