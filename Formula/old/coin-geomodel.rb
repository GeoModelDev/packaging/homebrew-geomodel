class CoinGeomodel < Formula
  desc "The SoQt package, allowing Coin to be used with Qt"
  homepage "https://bitbucket.org/Coin3D/coin"

  # SOURCES
  url "https://atlas-vp1.web.cern.ch/atlas-vp1/sources/coin_c8a8003d4_1Dec2020.zip"
  sha256 "39b02f84b1f252fcada10e2a4836fc80375c2788696863d9cb1710526d5a17bd"

  # DEPENDENCIES
  depends_on "atlas/geomodel/cmake-geomodel" => :build
  depends_on "atlas/geomodel/simage-geomodel"

  # BUILD INSTRUCTIONS
  def install
    mkdir "builddir" do
      system "cmake", "..", *std_cmake_args
      system "make"
      system "make", "install"
    end
  end

  # PRE-COMPILED PACKAGES
  bottle do
    root_url "http://atlas-vp1.web.cern.ch/atlas-vp1/sources/bottles"
    cellar :any
    sha256 "a085aaee16848d6b3e753c3862d08847f68dd218ede35ab2af2ac73f14cde9ca" => :mojave
  end

end
