class Geomodelg4 < Formula

  desc "GeoModelG4"
  homepage "https://gitlab.cern.ch/GeoModelDev/GeoModelG4"

  # SOURCES
  url "https://gitlab.cern.ch/GeoModelDev/GeoModelG4/-/archive/1.1.0/GeoModelG4-1.1.0.zip"
  sha256 "8812343a8fe37b9cc88459d761eec2f4dfa74a6f01693ef261d9f3bf4c620d1f"


  head do
    url "https://gitlab.cern.ch/GeoModelDev/GeoModelG4.git"
  end



  # DEPENDENCIES
  depends_on "cmake" => :build
  depends_on "eigen"
  depends_on "atlas/geomodel/geomodel"
  depends_on "atlas/geomodel/geant4"

  # INSTALLATION INSTRUCTIONS
  def install
    mkdir "build" do
      system "cmake", "-G", "Unix Makefiles", "..", *std_cmake_args
      system "make"
      system "make", "install"
   end
  end

 bottle do
    root_url "http://atlas-vp1.web.cern.ch/atlas-vp1/sources/bottles"
    cellar :any
    # sha256 "8e65e7dceac0556a7bf00f51bd66037ffb5bd0c45c7288aae4142031c57a7588" => :catalina
    sha256 "2ec39284cfba151da3d60dc7d4c9b10109ba2dcc0e5a7ff50b4dbe0a146dd2b2" => :mojave
  end


end
