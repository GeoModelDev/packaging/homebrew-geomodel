# Documentation: https://docs.brew.sh/Formula-Cookbook
#                https://www.rubydoc.info/github/Homebrew/brew/master/Formula
class Handsonplugins < Formula
  desc "Some very simple plugins to exercise the plugins methods"
  homepage "https://gitlab.cern.ch/GeoModelATLAS/HandsOnPlugins"

  # SOURCES

  url "https://gitlab.cern.ch/GeoModelATLAS/handsonplugins/-/archive/1.0.0/handsonplugins-1.0.0.tar.gz"
  sha256 "0bdf44dbfaeaf7e2872fbea52a1f8321fd816d55d2fb282d3b8ab8d1a3738577"

  head do
    url "https://gitlab.cern.ch/GeoModelATLAS/HandsOnPlugins.git"
  end

  # DEPENDENCIES
  depends_on "cmake" => :build
  depends_on "eigen"
  depends_on "nlohmann-json"
  depends_on "atlas/geomodel/geomodelcore"
  depends_on "atlas/geomodel/geomodelio"

  # INSTALLATION INSTRUCTIONS
  def install
    mkdir "build" do
      system "cmake", "-G", "Unix Makefiles", "..", *std_cmake_args
      system "make"
      system "make", "install"
   end
  end

  bottle do
    root_url "http://atlas-vp1.web.cern.ch/atlas-vp1/sources/bottles"
    cellar :any
    # sha256 "13f6c905981260dad8ec2e3cab00e78b7aeaccb68a8aff93a3f2e67cc9a9e281" => :catalina
        sha256 "c49d624487815715e2933605217fa3162779213810b97393b6249938e3513225" => :mojave
  end

end
