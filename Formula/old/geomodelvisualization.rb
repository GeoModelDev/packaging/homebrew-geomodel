# Documentation: https://docs.brew.sh/Formula-Cookbook
#                https://www.rubydoc.info/github/Homebrew/brew/master/Formula
class Geomodelvisualization < Formula
  desc "The GeoModel Tools Suite for High-Energy-Physics Detector Description - Visualization"
  homepage "https://gitlab.cern.ch/GeoModelDev/GeoModel"

  # SOURCES
  url "https://gitlab.cern.ch/GeoModelDev/GeoModel/-/archive/4.1.1.1/GeoModel-4.1.1.1.zip"
  sha256 "201797937b7603e6c03b6efa71181451f9ee754f6b8bbfbe8ee727d9a86726ff"

  head do
    url "https://gitlab.cern.ch/GeoModelDev/GeoModel.git"
  end

  # DEPENDENCIES
  depends_on "cmake" => :build
  depends_on "qt"
  depends_on "eigen"
  depends_on "nlohmann-json"
  depends_on "atlas/geomodel/simage-geomodel"
  depends_on "atlas/geomodel/coin-geomodel"
  depends_on "atlas/geomodel/soqt-geomodel"
  depends_on "atlas/geomodel/geomodel"

  
  # INSTALLATION INSTRUCTIONS
  def install
    mkdir "build" do
      system "cmake", "-G", "Unix Makefiles", "../GeoModelVisualization", *std_cmake_args
      system "make"
      system "make", "install"
   end
  end

  # PRE-COMPILED PACKAGES (Bottles)
  bottle do
    root_url "http://atlas-vp1.web.cern.ch/atlas-vp1/sources/bottles"
    sha256 "4844b462974d93cafe309e35479033ffed2f60664a944be64d9ab795a5665adc" => :mojave
  end

  # test do
  #   # `test do` will create, run in and delete a temporary directory.
  #   #
  #   # This test will fail and we won't accept that! For Homebrew/homebrew-core
  #   # this will need to be a test that verifies the functionality of the
  #   # software. Run the test with `brew test geomodelkernel`. Options passed
  #   # to `brew install` such as `--HEAD` also need to be provided to `brew test`.
  #   #
  #   # The installed folder is not in the path, so use the entire path to any
  #   # executables being tested: `system "#{bin}/program", "do", "something"`.
  #   system "false"
  # end
end
