class SimageGeomodel < Formula
  desc "The simage library for texture mapping in 3D graphics applications"
  homepage "https://grey.colorado.edu/coin3d/index.html"

  # SOURCES
  url "http://cern.ch/atlas-software-dist-eos/externals/Simage/Coin3D-simage-2c958a61ea8b.zip"
  sha256 "5e124c433bb70e4eadd8990c07c2d75ec3f5bb3ba22fec5c4318a00ae7fe90ae"

  # COMPILATION INSTRUCTIONS
  def install
    system "./configure", "--disable-debug",
                          "--disable-dependency-tracking",
                          "--disable-silent-rules",
                          "--prefix=#{prefix}"
    system "make", "install" # if this fails, try separate make/make install steps
  end

  # PRE-COMPILED PACKAGES (Bottles)
  bottle do
    root_url "http://atlas-vp1.web.cern.ch/atlas-vp1/sources/bottles"
    cellar :any
    rebuild 1
    sha256 "1704ba8173fd929de6048058d8140cf7c0195eeb52b5cc8b653953e714114e08" => :mojave
  end

end
