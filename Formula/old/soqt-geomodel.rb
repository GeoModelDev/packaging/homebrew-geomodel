class SoqtGeomodel < Formula
  desc "The SoQt package, allowing Coin to be used with Qt"
  homepage "https://bitbucket.org/Coin3D/SoQt"

  # SOURCES
  url "http://atlas-vp1.web.cern.ch/atlas-vp1/sources/soqt_5796270_1Dec2020.zip"
  sha256 "9c64bf12dc69599793a2d4a95aa406fa64f12a3ffd9ad20d301681a7250bced6"

  #head "https://bitbucket.org/rmbianchi/soqt",

  # DEPENDENCIES
  depends_on "atlas/geomodel/cmake-geomodel" => :build
  depends_on "qt5"
  depends_on "atlas/geomodel/coin-geomodel"

  # BUILD INSTRUCTIONS
  def install
    mkdir "builddir" do
      system "cmake", "-DSOQT_USE_X11=0", "..", *std_cmake_args
      system "make"
      system "make", "install"
    end
  end

  # PRE-COMPILED PACKAGES
  bottle do
    root_url "http://atlas-vp1.web.cern.ch/atlas-vp1/sources/bottles"
    cellar :any
    sha256 "e629af53f8e71901fb7758310b253b2fa76583cda2e899992b4e7e1e5a6f2258" => :mojave
  end

end
