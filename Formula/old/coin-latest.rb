class CoinLatest < Formula
  desc "The SoQt package, allowing Coin to be used with Qt"
  homepage "https://bitbucket.org/Coin3D/coin"

  # SOURCES
  url "https://atlas-vp1.web.cern.ch/atlas-vp1/sources/coin-4.0.0-src_acbf0d4_2020Apr12.zip"
  sha256 "634e2a81a2403519f3d8160ad76a74bd4f7a8f6721b93d9020c1dfa7b958045c"

  # DEPENDENCIES
  depends_on "atlas/geomodel/cmake-geomodel" => :build
  depends_on "atlas/geomodel/simage-geomodel"

  # BUILD INSTRUCTIONS
  def install
    mkdir "builddir" do
      system "cmake", "..", *std_cmake_args
      system "make"
      system "make", "install"
    end
  end

  # PRE-COMPILED PACKAGES
  bottle do
    root_url "http://atlas-vp1.web.cern.ch/atlas-vp1/sources/bottles"
    cellar :any
    sha256 "8f66369c0a30f2926d476817a4ef18bb45feb1115f3aa271a3feb7fad8433bd9" => :mojave
  end

end
