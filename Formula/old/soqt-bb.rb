class SoqtBb < Formula
  desc "The SoQt package, allowing Coin to be used with Qt"
  homepage "https://bitbucket.org/Coin3D/SoQt"

  # SOURCES
  #url "https://bitbucket.org/rmbianchi/soqt/get/mac-mojave-fix.zip"
  url "https://bitbucket.org/rmbianchi/soqt/downloads/soqt-1b4fe9d-macos-mojave-fix.zip"
  sha256 "1649c25c660d1bfa5e698de5ceffd470483a2dc26f55c85cdd8bc2164b0b0814"

  head "https://bitbucket.org/rmbianchi/soqt",
      :using    => :hg

  # DEPENDENCIES
  depends_on "cmake" => :build
  depends_on "qt5"
  depends_on "coin-bb"

  # BUILD INSTRUCTIONS
  def install
    mkdir "builddir" do
      system "cmake", "..", *std_cmake_args
      system "make"
      system "make", "install"
    end
  end

  # PRE-COMPILED PACKAGES
  bottle do
    root_url "http://atlas-vp1.web.cern.ch/atlas-vp1/sources/bottles"
    cellar :any
    rebuild 1
    sha256 "c12d0e7b94c5b18b442fb941e64ce481011ee43fa3e8c41fcd125de88697eb75" => :mojave
  end

end
