class CoinBb < Formula
  desc "The SoQt package, allowing Coin to be used with Qt"
  homepage "https://bitbucket.org/Coin3D/coin"

  # SOURCES
  url "https://bitbucket.org/Coin3D/coin/downloads/coin-4.0.0-src.zip"
  sha256 "33e3484d05067ab86450e286869ed45f2bbf24d140adaf5120511d8d42c65658"

  head "https://bitbucket.org/Coin3D/coin",
      :using    => :hg

  # DEPENDENCIES
  depends_on "cmake" => :build
  # depends_on "simage"

  # BUILD INSTRUCTIONS
  def install
    mkdir "builddir" do
      system "cmake", "..", *std_cmake_args
      system "make"
      system "make", "install"
    end
  end

  # PRE-COMPILED PACKAGES
  bottle do
    root_url "http://atlas-vp1.web.cern.ch/atlas-vp1/sources/bottles"
    cellar :any
    rebuild 1
    sha256 "18a28821a9f7a882bbe6eaa58bf1f816779d5f9b74ee308d3b6e832534adc9ff" => :mojave
  end

end
