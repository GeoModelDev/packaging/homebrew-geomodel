class GeomodelFullsimlight < Formula

  desc "The GeoModel Tools Suite for High-Energy-Physics Detector Description - FullSimLight"
  homepage "https://gitlab.cern.ch/GeoModelDev/GeoModel"

  
  # SOURCES
  url "https://gitlab.cern.ch/GeoModelDev/GeoModel/-/archive/6.0.0/GeoModel-6.0.0.zip"
  sha256 "d50538507945002a2f4f2a874b993b39237775ccaf7149565cd655b829885615"


  head do
    url "https://gitlab.cern.ch/GeoModelDev/GeoModel.git"
  end

  # DEPENDENCIES
  depends_on "cmake" => :build
  depends_on "eigen"
  depends_on "nlohmann-json"
  depends_on "hdf5"
  depends_on "atlas/geomodel/geomodel"
  depends_on "atlas/geomodel/geomodel-geomodelg4"
  depends_on "atlas/geomodel/geomodel-thirdparty-geant4" 
  
  # TODO: make this an option for ATLAS users
  #depends_on "atlas/geomodel/geomodel-extensions-atlaslar" 
  #depends_on "atlas/geomodel/geomodel-extensions-atlas" 
  
  # TODO: make this option change the build, as in FSL
  option "without-hepmc3", "Compile without HepMC3 support. FullSimLight will be fully usable, but you will not be able to use HepMC3 input." 
  option "without-pythia", "Compile without Pythia support. FullSimLight will be fully usable, but you will not be able to use the Pythia interface." 
  
  # A NOTE: GeoModel uses customized formula to bottle HepMc3 and Pythia,
  # to always have well tested versions for the up to date GeoModel tools
  # and all needed OS versions and platforms.
  depends_on "geomodel-thirdparty-hepmc3" => :recommended 
  depends_on "geomodel-thirdparty-pythia" => :recommended 


  # INSTALLATION INSTRUCTIONS
  def install
    args = %w[
      -DGEOMODEL_USE_HEPMC3=ON
      -DGEOMODEL_USE_PYTHIA=ON
    ]
    #args << "-DCMAKE_INSTALL_RPATH=#{rpath}"
    
    mkdir "build" do
      system "cmake", "-G", "Unix Makefiles", "../FullSimLight", *args, *std_cmake_args
      system "make"
      system "make", "install"
   end
  end

 
  bottle do
    root_url "https://geomodel.web.cern.ch/sources/bottles"
    sha256 cellar: :any, arm64_ventura: "eeb421fcf1ccaf8df3c9a56975c5e030e7237dfbffe10850d261ed3ed7270c48" 
  end


end
