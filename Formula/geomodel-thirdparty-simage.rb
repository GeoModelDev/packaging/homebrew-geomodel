class GeomodelThirdpartySimage < Formula
  desc "The simage library for texture mapping in 3D graphics applications"
  homepage "https://grey.colorado.edu/coin3d/index.html"

  # SOURCES
  url "http://cern.ch/atlas-software-dist-eos/externals/Simage/simage-1.8.1-src.zip"
  sha256 "308a8712c1f28cf6e662acab2e1fdd263fbfcb11323869806f4fef435653d4d3"

  # DEPENDENCIES
  depends_on "cmake" => :build

  # COMPILATION INSTRUCTIONS
  def install
    mkdir "builddir" do
      system "cmake", "-DSIMAGE_BUILD_DOCUMENTATION=0", "-DSIMAGE_BUILD_EXAMPLES=0", "-DSIMAGE_LIBSNDFILE_SUPPORT=0", "-DSIMAGE_MPEG2ENC_SUPPORT=0", "-DSIMAGE_OGGVORBIS_SUPPORT=0", "..", *std_cmake_args
      system "make"
      system "make", "install"
    end
  end

  # PRE-COMPILED PACKAGES (Bottles)
  bottle do
    root_url "https://geomodel.web.cern.ch/sources/bottles"
    sha256 cellar: :any, mojave: "96e1c489948296c6a9649d9a833e01950494fae41ca7c2f4bd55a51c5ac39ca9"
    sha256 cellar: :any, catalina: "ebd7142293c9afc3dcc69a32d1cb035d476f0bcf60c43b1a2da354b6f89dcf97"
    sha256 cellar: :any, big_sur: "86249b521f0f142ebf02b0d56d71f15a4d15291efb85a0ea232f7e5256d72abf"
    sha256 cellar: :any, arm64_monterey: "64c661aef4cd46c3fd1153b6c479b56979281e12450f85136098e630ef2d0c49"
  end  


end
