class GeomodelExtensionsAtlas < Formula
  desc "The GeoModel Tools Suite for High-Energy-Physics Detector Description - Extensions for the ATLAS experiment"
  homepage "https://gitlab.cern.ch/GeoModelDev/GeoModel"
  license "Apache-2.0"
  # SOURCES 
  # GitHub's archives lack submodules, must pull:
  url "https://gitlab.cern.ch/GeoModelDev/GeoModel.git",
      revision: "3dd09442ad1c9898b3dcfbb332d816fe2ebfc0fd"
  version "6.3.0" # patching the GeoModel 4.4.5 tag with a new version of the ATLASExtensions 
  #head "https://gitlab.cern.ch/GeoModelDev/GeoModel.git", branch: "master"

  # DEPENDENCIES
  depends_on "cmake" => :build
  depends_on "geomodel-fullsimlight" #MagField plugin needs FullSimLight headers

  # Fetch the ATLAS Magnetic Field data for the post_install step
  resource "atlasmagfielddata" do 
  url "https://geomodel.web.cern.ch/atlas-magnetic-field/bmagatlas_09_fullAsym20400.data"
  sha256 "e4cc6555c3650c8ce8da543ac155b101d3b6295a2d7271c0bada30a9783b8400"
  end

  # INSTALLATION INSTRUCTIONS
  def install
    mkdir "build" do
      system "cmake", "-G", "Unix Makefiles", "../ATLASExtensions", *std_cmake_args
      system "make"
      system "make", "install"
   end
  end

  # POST-INSTALL
  # This is perfomed both after an installation from source and from a pre-compiled bottle
  # We use this step to install the ATLAS Magnetic Field data file into the "share" folder,
  # for the ATLASMagFieldPlugin to find it.
  def post_install
    resource("atlasmagfielddata").stage {system "cp", Dir["*"].first, share/"FullSimLight/ATLAS"} 
  end

  # PRE-COMPILED PACKAGES (Bottles)
  bottle do
    root_url "https://geomodel.web.cern.ch/sources/bottles"
    sha256 arm64_ventura: "6714c31319794cffa8ad39e084f8a4eb25bd4f8dd7282ada814109cdb366db95"
  end

  # test do
  #   # `test do` will create, run in and delete a temporary directory.
  #   #
  #   # This test will fail and we won't accept that! For Homebrew/homebrew-core
  #   # this will need to be a test that verifies the functionality of the
  #   # software. Run the test with `brew test geomodelkernel`. Options passed
  #   # to `brew install` such as `--HEAD` also need to be provided to `brew test`.
  #   #
  #   # The installed folder is not in the path, so use the entire path to any
  #   # executables being tested: `system "#{bin}/program", "do", "something"`.
  #   system "false"
  # end
end
